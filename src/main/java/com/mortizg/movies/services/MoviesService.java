package com.mortizg.movies.services;

import com.mortizg.movies.dao.MoviesDAO;
import com.mortizg.movies.dto.MoviePartial;
import com.mortizg.movies.entities.Movies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MoviesService {

    @Autowired
    private MoviesDAO moviesDAO;

    public List<MoviePartial> getMovies() {
        List<Movies> allMovies = moviesDAO.findAll();
        return mappingMoviesPartialDTO(allMovies);
    }

    public Movies getMovie(Long idMovie) {
        Optional<Movies> optionalMovieById = moviesDAO.findById(idMovie);
        return optionalMovieById.isPresent() ? optionalMovieById.get() : null;
    }

    private List<MoviePartial> mappingMoviesPartialDTO(List<Movies> movies) {
        List<MoviePartial> moviesPartial = new ArrayList<>();
        for(Movies movie : movies){
            moviesPartial.add(mappingMoviePartialDTO(movie));
        }

        return moviesPartial;
    }

    private MoviePartial mappingMoviePartialDTO(Movies movie) {
        MoviePartial moviePartial = new MoviePartial();
        moviePartial.setTitle(movie.getTitle());
        moviePartial.setMovieYear(movie.getMovieYear());

        return moviePartial;
    }
}
