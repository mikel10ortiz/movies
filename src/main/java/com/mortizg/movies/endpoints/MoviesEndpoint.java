package com.mortizg.movies.endpoints;

import com.mortizg.movies.dto.MoviePartial;
import com.mortizg.movies.entities.Movies;
import com.mortizg.movies.services.MoviesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MoviesEndpoint {

    @Autowired
    private MoviesService moviesService;

    @GetMapping
    public List<MoviePartial> getMovies() {
        return moviesService.getMovies();
    }

    @GetMapping("{idMovie}")
    public Movies getMovieById(@PathVariable Long idMovie) {
        return moviesService.getMovie(idMovie);
    }
}
