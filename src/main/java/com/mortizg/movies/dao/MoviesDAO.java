package com.mortizg.movies.dao;

import com.mortizg.movies.entities.Movies;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoviesDAO extends JpaRepository<Movies, Long> {
}
