package com.mortizg.movies.service;

import com.mortizg.movies.services.MoviesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MoviesServiceTest {

    @Autowired
    private MoviesService moviesService;

    @Test
    public void getMoviesTest_isOne() {
        Assertions.assertEquals(moviesService.getMovies().size(), 1);
    }

    @Test
    public void getMoviesByIdTest_hasCorrectTitle() {
        Assertions.assertEquals(moviesService.getMovie(1l).getTitle(), "TITLE 1");
    }
}
